# Hrypton py

Access to your passwords anywhere and anytime without saving anything!

**NOTE:** This is an unofficial python version of Hrypton. To see more details
about Hrypton visit [hrypton.ir](https://hrypton.ir).

## Installing

(You must have python3)

1. Clone the repository:
```
git clone https://codeberg.org/mskf1383/Hrypton-py.git
```
2. Go to cloned repository directory
```
cd Hrypton-py
```
3. Run this command to install it:
```
python3 setup.py install
```

## Using

```
hrypton <URL> <Username> <How_many_time_changed>
```

> Then it asked you for your master password.

## License

[![GNU GPL v3](./LICENSE.png)](./LICENSE)