from setuptools import setup

setup(
    name='Hrypton py',
    version='0.1.1',
    description='Access to your passwords anywhere and anytime without saving anything!',
    scripts= ['hrypton']
)
